var mongoose = require("mongoose");
var conekta = require("conekta");
conekta.api_key = process.env.CONEKTA_API_KEY;
var Schema = mongoose.Schema;

var UserSchema = new Schema({
  twitterId: {type: String, required: true, index: {unique: true}},
  twitterUsername: String,
  twitterToken: {type: String},
  twitterSecret: {type: String},
  customerId: {type: String},
  isAdmin: {type: Boolean, default: false}
});

var createCustomer = function(name, email, cardToken, cb) {
  conekta.Customer.create({
    'name': name,
    'email': email,
    'cards': [cardToken]
  }, function(err, customer) {
    if (err) {
      cb(err);
    } else {
      cb(null, customer._json);
    }
  });
};

UserSchema.methods.findCustomer = function(cb) {
  conekta.Customer.find(this.customerId, function(err, customer) {
    if (err) {
      cb(err);
    } else {
      cb(null, customer);
    }
  });
};

UserSchema.methods.updateCustomer = function(name, email, cardToken, cb) {
  this.findCustomer(function(err, customer) {
    if (err) {
      if (err.http_code == 404) {
        createCustomer(name, email, cardToken, function(err, customer) {
          if(err) {
            cb(err);
          } else {
            cb(null, customer.id);
          }
        });
      } else {
        cb(null, customer.id);
      }
    } else {
      var payload = {};
      if (name) payload.name = name;
      if (email) payload.email = email;
      
      customer.update(payload, function(err, userRes) {
        if (err) {
          cb(err);
        } else {
          customer.cards[0].update({
            token: cardToken,
            active: true
          }, function(err, cardRes) {
            if (err) {
              cb(err);
            } else {
              cb(null, userRes._id);
            }
          });
        }
      });
    }
  });
};

UserSchema.methods.makeCustomer = function(name, email, cardToken, cb) {
  createCustomer(name, email, cardToken, function(err, customer) {
    if (err) {
      cb(err);
    } else {
      cb(null, customer.id);
    }
  });
};

module.exports = mongoose.model('User', UserSchema);