var express = require("express");
var User    = require("../models/user.js");

var auth = function(req, res, next) {
  if (req.isAuthenticated()) {
    next();
  } else {
    res.sendStatus(401);
  }
};
var hasAccess = function(req, res, next) {
  if(req.user && 
      (req.user.isAdmin || req.user.twitterId == req.params.userTwitterId)) {
    next();
  } else {
    res.sendStatus(401);
  }
};

module.exports = function(app, passport) {
  var authRouter = express.Router();
  
  authRouter.get('/loggedin', function(req, res) { 
    res.send(req.isAuthenticated() ? req.user : '0'); 
  });
  
  authRouter.post('/logout', auth, function(req, res) {
    if (req.isAuthenticated()) {
      req.logout();
      res.json({ success: true, logedout: true });
    } else {
      res.json({ success: false, error: "Not logged In"});
    }
    
  });
  
  authRouter.get('/twitter', passport.authenticate('twitter'));
  
  authRouter.get('/twitter/callback', passport.authenticate('twitter', {
    successRedirect: '/pickup',
    failureRedirect: '/pickup'
  }));
  
  var apiRouter = express.Router();
  
  // What are these used for? Find out and delete if possible.
  apiRouter.get('/user/success', function(req, res) {
    res.json({ message: "logged in" });
  });
  
  apiRouter.get('/user/fail', function(req, res) {
    res.json({ message: "fail" });
  });
  
  // Used to test secure routes? Find out and delete if possible.
  apiRouter.get('/secure', auth, function(req, res) {
    res.json({ message: 'Access to secure route' })
  });
  
  // Get user list
  // apiRouter.get('/user', function(req, res) {
  //   User.find({}, function(err, users) {
  //     if (err) {
  //       res.json({error: err});
  //     } else {
  //       res.json(users);
  //     }
  //   });
  // });
  
  // Get user by Twitter ID
  apiRouter.get('/user/:userTwitterId', hasAccess, function(req, res) {
    // console.log(req.session);
    
    User.findOne({ twitterId: req.params.userTwitterId }, function(err, user) {
      if (err) {
        res.json({error: err});
      } else {
        res.json(user);
      }
    });
  });
  
  // Update user. As users are bounded to a twitter account this is only relevant for the credit card.
  apiRouter.put('/user/:userTwitterId', hasAccess, function(req, res) {
    User.findOne({ twitterId: req.params.userTwitterId }, function(err, user) {
      if (err) {
        res.json({ error: err });
      } else {
        if(user.customerId) {
          user.updateCustomer(req.body.name, req.body.email, req.body.token, function(err, customerId) {
            if (err) {
              res.json({ error: err });
            } else {
              console.log(customerId);
              user.customerId = customerId;
              user.save(function(err) {
                res.json({ sucess: true });
              });
            }
          });
        } else {
          user.makeCustomer(req.body.name, req.body.email, req.body.token, function(err, customerId) {
            if (err) {
              res.json({ error: err });
            } else {
              user.customerId = customerId;
              user.save(function(err) {
                if (err) {
                  res.json({ error: err})
                } else {
                  res.json({ sucess: true });
                }
              });
            }
          });
        }
      }
    });
  });
  
  // Know if user is client
  apiRouter.get('/user/:userTwitterId/isclient', hasAccess, function(req, res) {
    User.findOne({ twitterId: req.params.userTwitterId }, function(err, user) {
      var response = false;
      
      if (user.customerId) {
        response = true
      }
      res.json({ isClient: response, error: err });
    });
  });

  app.use('/auth', authRouter);
  app.use('/api', apiRouter);
}