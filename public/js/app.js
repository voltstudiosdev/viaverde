/* global angular */

angular.module('viaVerdeApp', ['viaVerdeRoutes', 'viaVerdeDirectives'])

.controller('mainController', ['$scope', '$location', '$anchorScroll', 
function($scope, $location, $anchorScroll) {
    var mainCtrl = this;
    
    mainCtrl.goToVenues = function() {
      $location.url('/#venue-info');
      $anchorScroll();
    };
}])

.controller('homeController', function($scope, $location, $anchorScroll) {
})

.controller('loginCtrl', ['$window', function($window) {
  var login = this;
  
  login.twitterLogin = function() {
    $window.location.assign('/auth/twitter');
  };
}])

.controller('pickupCtrl', ['$state', '$http', 'LoggedIn', 'IsClient', 
  function($state, $http, LoggedIn, IsClient) {
    
  var pickup = this;
  pickup.formData = {};

  LoggedIn().then(function(user) {
    pickup.formData.twitterId = user.twitterId;
    console.log("Logged in checked");
    IsClient(user.twitterId).then(function(isClient) {
      console.log(isClient);
      pickup.twitterUsername = user.twitterUsername;
      $state.go('.ready');
    }, function(err) {
      console.log(err);
      pickup.error = err.error;
      $state.go('.creditcard');
    })
  }, function() {
    $state.go('.login');
  });
  
  pickup.logout = function() {
    $http.post('/auth/logout').then(function(response) {
      $state.go('pickup.login');
    }, function(err) {
      pickup.error = err.error
    })
  }
}])

.controller('creditCardCtrl', ['$scope', '$http', '$state', 'ConektaService', function($scope, $http, $state, ConektaService) {
  var cc = this;
  cc.userData = $scope.$parent.pickup.formData;
  cc.card = {};
  cc.months = [];
  cc.years = [];
  for (var i = 1; i <= 12; i++) {
    cc.months.push(i);
  }
  for (var i = new Date().getFullYear(), e = new Date().getFullYear() + 25; i <= e; i++) {
    cc.years.push(i);
  }
  
  cc.tokenizeCard = function() {
    ConektaService.setKey('key_KwYmMbLrUZybxs5JViqyKyw');
    ConektaService.tokenizeCard({ card: cc.card }).then(function(token) {
      var payload = {token: token.id, email: cc.userData.email, name: cc.userData.name};
      $http.put('/api/user/' + cc.userData.twitterId, payload).then(function(message) {
        $state.go('pickup.ready');
      }, function(err) {
        console.log(err);
      });
    }, function(err) {
      cc.error = err;
    });
  }
}])

.controller('readyCrtl', ['$state', function($state) {
  var ready = this;
  
  ready.goEdit = function() {
    $state.go('^.creditcard');
  };
}])

.controller('menuCtrl', function() {
  var menu = this;
  menu.menus = [{name: 'Ensaladas', products: [{name: 'Macedonia'}, {name: 'Caprese'}]},
                {name: 'Desayunos', products: [{name: 'Veggie'}, {name: 'Bisquets'}]}];
});