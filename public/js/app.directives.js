angular.module('viaVerdeDirectives', [])

.directive('menuCard', function() {
  return {
    restrict: 'E',
    scope: {
      menu: '=menu'
    },
    templateUrl: 'partials/menu_card.html',
    controllerAs: 'menuCtrl',
    controller: function($scope) {
      var menuCtrl = this;
      
      menuCtrl.featuredProduct = "No featuredProduct";
      
      menuCtrl.changeFeatured = function(product) {
        menuCtrl.featuredProduct = product;
      }
    }
  }
});