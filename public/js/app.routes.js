/*global angular*/
angular.module('viaVerdeRoutes', ['ui.router'])

.config(function($stateProvider, $urlRouterProvider, $locationProvider) {
  $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'views/home.html',
      controller: 'homeController as homeCtrl'
    })
    
    .state('menu', {
      url: '/menu',
      templateUrl: 'views/menu.html'
    })
    
    .state('pickup', {
      url: '/pickup',
      templateUrl: 'views/pickup.html',
      controller: 'pickupCtrl',
      controllerAs: 'pickup'
    })
    
    .state('pickup.login', {
      templateUrl: 'views/pickup.login.html',
      controller: 'loginCtrl',
      controllerAs: 'login'
    })
    
    .state('pickup.creditcard', {
      templateUrl: 'views/pickup.creditcard.html',
      controller: 'creditCardCtrl',
      controllerAs: 'cc'
    })
    
    .state('pickup.ready', {
      templateUrl: 'views/pickup.ready.html',
      controller: 'readyCrtl',
      controllerAs: 'ready'
    })
    
    .state('order', {
      url: '/order',
      templateUrl: 'views/order.html',
      controller: 'orderController as order'
    })
    
    .state('order.login', {
      
    })
    
    $locationProvider.html5Mode(true);
});