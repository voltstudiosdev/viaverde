/* global angular */
/* global Conekta */

var conektaObject = function($q) {
  this.setKey = function(key) {
    Conekta.setPublishableKey(key);
  };
  
  this.getKey = function() {
    return Conekta.getPublishableKey();
  };
  
  this.tokenizeCard = function(tokenParams) {
    if (!Conekta.card.validateNumber(tokenParams.card.number)) {
      throw "Card Number Invalid"
    } else if (!Conekta.card.validateExpirationDate(tokenParams.card.exp_month, tokenParams.card.exp_year)) {
      throw "Expiration Date Invalid"
    } else if (!Conekta.card.validateCVC(tokenParams.card.cvc)) {
      throw "CVC invalid"
    } else {
      return $q(function(resolve, reject) {
        Conekta.token.create(tokenParams, function(token) {
          resolve(token);
        }, function(err) {
          console.log(err);
          reject(err);
        });
      });
    }
  };
};

angular.module('viaVerdeApp')

.factory('LoggedIn', ['$q', '$http', function LoggedInFactory($q, $http) {
  return function() {
    var deferred = $q.defer();
    
    $http.get('/auth/loggedin').then(function(response) {
      if (response.data != 0) {
        deferred.resolve(response.data);
      } else {
        deferred.reject();
      }
    });
    
    return deferred.promise;
  };
}])

.factory('IsClient', ['$q', '$http', function IsClientFactory($q, $http) {
  return function(twitterId) {
    return $q(function(resolve, reject) {
      $http.get('/api/user/' + twitterId + '/isclient').then(function(response) {
        console.log(response.data);
        if (response.data.isClient) {
          resolve(response.data.isClient);
        } else {
          reject(response.data.isClient)
        }
      }, function(err) {
        reject(err.error);
      });
    });
  };
}])

.service('ConektaService', conektaObject);
