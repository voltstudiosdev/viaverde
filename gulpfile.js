'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var concat = require('gulp-concat');

var config = {
  bootstrapDir: './bower_components/bootstrap-sass',
  publicDir: './public',
};
 
gulp.task('sass', function () {
  return gulp.src('./public/sass/**/*.scss')
    .pipe(sass({
      includePaths: [config.bootstrapDir + '/assets/stylesheets']
    }).on('error', sass.logError))
    .pipe(concat('app.css'))
    .pipe(minifyCss({keepSpecialComments: 0}))
    .pipe(gulp.dest(config.publicDir + '/css'));
});

gulp.task('fonts', function() {
    return gulp.src(config.bootstrapDir + '/assets/fonts/**/*')
    .pipe(gulp.dest(config.publicDir + '/fonts'));
});
 
gulp.task('sass:watch', ['fonts'], function () {
  gulp.watch('./public/sass/**/*.scss', ['sass']);
});