var User = require('../app/models/user');
var TwitterStrategy = require('passport-twitter').Strategy;

module.exports = function(passport) {
  passport.serializeUser(function(user, done) {
      done(null, user.id, user.twitterId);
  });
  
  passport.deserializeUser(function(id, done) {
      User.findById(id, function(err, user) {
          done(err, user);
      });
  });
  
  passport.use(new TwitterStrategy({
      consumerKey: process.env.CONSUMER_KEY,
      consumerSecret: process.env.CONSUMER_SECRET,
      callbackURL: process.env.CALLBACK_URL
  },
  function(token, tokenSecret, profile, done) {
    User.findOne({ 'twitterId' : profile.id }, function(err, user) {
      if (err)
        return done(err);
          
      if (user) {
        return done(null, user);
      } else {
        var newUser = new User();
        
        newUser.twitterId = profile.id;
        newUser.twitterUsername = profile.username;
        newUser.twitterToken = token;
        
        newUser.save(function(err) {
          if (err) throw err;
          return done(null, newUser);
        });
      }
    });
  }));
}