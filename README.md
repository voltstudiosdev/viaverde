# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This application is for a food/beverage chain that is located across Mexico. 

The application provides general brand information, franchise information (pending on the information from the owners), and allows a person to order for pick-up via Twitter. 

The Twitter to order functionality is based on Angular and is unique to this application.


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact