var express = require("express");
var app = express();

var path = require("path");
var mongoose = require("mongoose");
var passport = require("passport");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var session = require("express-session");
var mongoDBStore = require("connect-mongodb-session")(session);
var configDB = require("./config/database.js");

var publicPath = path.join(__dirname, '/public');
mongoose.connect(configDB.uri);
var store = new mongoDBStore({
    uri: configDB.uri,
    collection: 'sessions' 
});

app.use(express.static(publicPath));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    store: store}));
    
require("./config/passport.js")(passport);
app.use(passport.initialize());
app.use(passport.session());

require("./app/routes/api.js")(app, passport);

app.get("*", function(req, res) {
	res.sendFile(publicPath + '/index.html');
});

app.listen(process.env.PORT, process.env.IP);
console.log('App running on port: ' + process.env.PORT);
